-- Create a new image record
INSERT INTO image ("created_at", "updated_at", "name", "user", "description", "published")
VALUES (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, $1, $2, $3, $4) RETURNING id;