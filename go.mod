module gitlab.com/tin-roof/meerchat-aac/image

go 1.20

require (
	github.com/google/uuid v1.3.0
	gitlab.com/tin-roof/meerchat-aac/database v1.1.0
	gitlab.com/tin-roof/meerchat-aac/logger v0.0.10
	gopkg.in/kothar/go-backblaze.v0 v0.0.0-20210124194846-35409b867216
)

require (
	github.com/golang/glog v1.0.0 // indirect
	github.com/google/readahead v0.0.0-20161222183148-eaceba169032 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20221227161230-091c0ba34f0a // indirect
	github.com/jackc/pgx/v5 v5.4.3 // indirect
	github.com/pquerna/ffjson v0.0.0-20190930134022-aa0246cd15f7 // indirect
	github.com/slack-go/slack v0.12.2 // indirect
	golang.org/x/crypto v0.9.0 // indirect
	golang.org/x/text v0.9.0 // indirect
)
