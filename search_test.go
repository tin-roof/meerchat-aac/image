package image

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"gitlab.com/tin-roof/meerchat-aac/database/query"
)

func TestBuildSearchQuery(t *testing.T) {

	// profileID := uuid.New()
	userID := uuid.New()

	searchParams := SearchParams{
		Keyword: "test",
		// Profile: &profileID,
		User:  &userID,
		Limit: 5,
		Page:  1,
		Order: query.Order{
			Direction: query.DirectionDESC,
			Field:     "created_at",
		},
	}

	expected :=
		`SELECT 
			p."id",
			p."user",
			p."title",
			p."buttons",
			p."grid",
			p."published",
			p."description"
		FROM "page" p
		WHERE 
			p."deleted" = FALSE
			AND (p."user" = $1 OR p."user" = $2 OR p."published" = TRUE)
			AND (p."title" LIKE '%' || $3 || '%' OR p."description" LIKE '%' || $3 || '%')
		ORDER BY p."created_at" DESC
		LIMIT 5 OFFSET 0;`

	query, params, err := buildSearchQuery(context.Background(), searchParams)

	if err != nil {
		t.Error("error was returned")
	}

	if query != expected {
		t.Error("query is malformed")
	}

	if len(params) != 3 {
		t.Error("not enough params returned")
	}
}
