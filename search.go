package image

import (
	"context"
	"fmt"

	"github.com/google/uuid"
	"gitlab.com/tin-roof/meerchat-aac/database"
	"gitlab.com/tin-roof/meerchat-aac/database/query"
	"gitlab.com/tin-roof/meerchat-aac/logger"
)

// SearchParams define the details of the search
type SearchParams struct {
	Keyword        string
	User           *uuid.UUID
	Limit          int
	Page           int
	Profile        *uuid.UUID
	Order          query.Order
	GetTotalImages bool
}

// Search uses params to find matching images in the db
func Search(ctx context.Context, db database.DB, params SearchParams) ([]*Image, int, error) {
	ctx, log := logger.New(ctx, "image.Search")

	log.Info(ctx, "searching for images")

	// @TODO
	query, queryParams, err := buildSearchQuery(ctx, params)
	if err != nil {
		log.WithError(err).Info(ctx, "failed to build image search query")
		return nil, 0, err
	}

	// lookup the images
	rows, err := db.Conn.Query(ctx, query, queryParams...)
	if err != nil {
		log.WithError(err).
			WithField("query", query).
			WithField("params", queryParams).
			Info(ctx, "error running image search lookup query")
		return nil, 0, err
	}

	// build the image list
	var totalImages int
	var images []*Image
	for rows.Next() {
		var i Image
		if err := rows.Scan(
			&i.ID,
			&i.Name,
			&i.Description,
			&totalImages,
		); err != nil {
			log.WithError(err).Info(ctx, "error scanning image lookup query row")
			continue
		}

		images = append(images, &i)
	}

	return images, totalImages, nil
}

func buildSearchQuery(ctx context.Context, params SearchParams) (string, []interface{}, error) {
	_, _ = logger.New(ctx, "image.buildSearchQuery")

	// @TODO: make sure the params are all defined properly to make the query

	keywordOrder := "$1"
	queryParams := []interface{}{}
	filterString := `i."published" = TRUE AND `
	getTotalImages := "0"

	// add the profile filter
	// user is optional here because the front end will be doing 2 different searches in order to find global images
	if params.User != nil {
		userFilter := `i."published" = FALSE AND (i."user" = $1`
		profileFilter := ""
		queryParams = append(queryParams, params.User.String())
		keywordOrder = "$2"

		if params.Profile != nil {
			profileFilter = `OR i."user" = $2`
			queryParams = append(queryParams, params.Profile.String())
			keywordOrder = "$3"
		}

		filterString = fmt.Sprintf(`%s %s) AND `, userFilter, profileFilter)
	}

	if params.GetTotalImages {
		getTotalImages = "count(*) OVER()"
	}

	// add the keyword to the query params
	queryParams = append(queryParams, params.Keyword)

	query := fmt.Sprintf(
		`SELECT 
			i."id",
			i."name",
			i."description",
			%s AS total
		FROM "image" i
		WHERE 
			%s
			i."description" LIKE '%c' || %s || '%c'
		ORDER BY i."%s" %s
		LIMIT %v OFFSET %v;`,
		getTotalImages,
		filterString,
		'%',
		keywordOrder,
		'%',
		params.Order.Field,
		params.Order.Direction,
		params.Limit,
		query.CalculateOffset(params.Page, params.Limit),
	)

	return query, queryParams, nil
}
