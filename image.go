package image

import (
	"bytes"
	"context"
	_ "embed"
	"errors"
	"fmt"
	"strings"
	"time"

	// vision "cloud.google.com/go/vision/apiv1"
	"github.com/google/uuid"
	"gitlab.com/tin-roof/meerchat-aac/database"
	"gitlab.com/tin-roof/meerchat-aac/logger"

	// gcoption "google.golang.org/api/option"
	backblaze "gopkg.in/kothar/go-backblaze.v0"
)

// queries needed for modifying boards
var (
	//go:embed queries/create.sql
	createQuery string
)

// Image defines the structure and settings of an image record
type Image struct {
	CreatedAt   *time.Time `json:"createdAt,omitempty"` // the date the image was created
	Description string     `json:"description"`         // user defined description of the image
	ID          *uuid.UUID `json:"id"`                  // id of the image record
	Image       []byte     `json:"image"`               // the actual image
	Image64     string     `json:"image64,omitempty"`   // the actual image as a base64 string
	MimeType    string     `json:"mimeType"`            // the mime type of the image
	Name        string     `json:"name"`                // user defined title of the image
	Published   bool       `json:"-"`                   // if the image is published or not
	Type        string     `json:"type,omitempty"`      // the type of image it is
	UpdatedAt   *time.Time `json:"updatedAt,omitempty"` // date the image was last updated
	User        *uuid.UUID `json:"-"`                   // user id of the user/profile that created the image

	loaded bool `json"-"` // if the image has been loaded from the db or not
}

// Delete an image from the DB and Backblaze
func (i *Image) Delete(ctx context.Context, db database.DB) {}

// Load image details from the DB
func (i *Image) Load(ctx context.Context, db database.DB) {}

// Save and image to the DB and Backblaze
func (i *Image) Save(ctx context.Context, db database.DB, bbClient *backblaze.B2) error {
	ctx, log := logger.New(ctx, "image.Save")

	log.Info(ctx, "saving a new image")

	// upload the image to Backblaze
	if err := i.upload(ctx, bbClient); err != nil {
		return err
	}

	// implement API to tag the image
	// @TODO: get this turned back on once i get the other saving stuff working
	// if err := i.tag(ctx); err != nil {
	// 	return err
	// }

	// save the image to the DB
	if err := db.Conn.QueryRow(ctx, createQuery, i.Name, i.User, i.Description, i.Published).Scan(&i.ID); err != nil {
		return err
	}

	// make sure the image has an id
	if i.ID == nil {
		return errors.New("failed to save the image to the db")
	}

	log.Info(ctx, "successfully saved image to the db")
	return nil
}

// tag use an API to tag and image so we can search it
// https://cloud.google.com/vision allows 1000 tags per month for free but then jumps to $0.004 per tag
// func (i *Image) tag(ctx context.Context, apiKey string) error {
// 	ctx, _ = logger.New(ctx, "image.tag")

// 	// get new gcp vision client
// 	client, err := vision.NewImageAnnotatorClient(ctx, gcoption.WithAPIKey(apiKey))
// 	if err != nil {
// 		return err
// 	}
// 	defer client.Close()

// 	// process the image
// 	image, err := vision.NewImageFromReader(bytes.NewReader(i.Image))
// 	if err != nil {
// 		return err
// 	}
// 	annotations, err := client.DetectLabels(ctx, image, nil, 10)
// 	if err != nil {
// 		return err
// 	}

// 	// get the annotations and add them to the image
// 	for _, annotation := range annotations {
// 		// check the score to make we are only using good annotations
// 		if annotation.Score < 0.8 {
// 			continue
// 		}

// 		i.Description = i.Description + " " + annotation.Description
// 	}

// 	return nil
// }

// upload an image to Backblaze
func (i *Image) upload(ctx context.Context, bbClient *backblaze.B2) error {
	ctx, log := logger.New(ctx, "image.upload")

	// set a default for the type
	if i.Type == "" {
		i.Type = "png"
	}

	// set a default mimeType
	if i.MimeType == "" {
		i.MimeType = "image/png"
	}

	// set the default user
	directory := "public"
	if i.User != nil {
		directory = strings.ReplaceAll(i.User.String(), "-", "")
	}

	// get the bucket
	bucket, err := bbClient.Bucket("meerchat")
	if err != nil {
		log.WithError(err).Info(ctx, "failed to get Backblaze bucket")
		return err
	}
	if bucket == nil {
		return errors.New("bucket not found")
	}

	log.WithField("bucket", bucket).Info(ctx, "found bucket")

	// generate the file name
	i.Name = fmt.Sprintf(
		"%s/%s.%s",
		directory,
		strings.ReplaceAll(uuid.NewString(), "-", ""),
		i.Type,
	)

	imagePath := fmt.Sprintf("images/%s", i.Name)
	log.WithField("imagePath", imagePath).Info(ctx, "got reader")

	// upload the file to Backblaze
	file, err := bucket.UploadTypedFile(imagePath, i.MimeType, make(map[string]string), bytes.NewReader(i.Image))
	if err != nil {
		log.WithError(err).Info(ctx, "failed to upload image")
		return err
	}

	log.WithField("fileID", file.ID).Info(ctx, "successfully uploaded file")
	return nil
}
